import Vapor

final class Routes: RouteCollection {
    let view: ViewRenderer
    init(_ view: ViewRenderer) {
        self.view = view
    }
    
    func build(_ builder: RouteBuilder) throws {
        /// GET /
        builder.get { req in
            return try self.view.make("welcome")
        }
        
        // response to requests to /info domain
        // with a description of the request
        builder.get("info") { req in
            return req.description
        }
        
        /// GET /dynamo/...
//        builder.resource("dynamo", DynamoController(view))
        
        /// GET /advancio/...
//        builder.resource("advancio", AdvancioController(view))
        
        /// GET /creatifside/...
//        builder.resource("creatifside", CreatifSideController(view))
        
        /// GET /cocoaheads/...
//        builder.resource("cocoaheads", CocoaheadsController(view))
        
        /// GET /talk/...
        builder.resource("cocoaheads-vapor", TalkController(view))
        
        builder.resource("tavla", TavlaController(view))
    }
}
